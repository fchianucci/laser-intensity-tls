% Dual (multi-otsu) and manual thresholding of intensity images
% The dual is then simplified to get gap and non-gap pixels
% fchianucci@gmail.com

%set the image path
results=[];
outs=[];
list=dir('*.jpg');

for i = 1:length(dir('*.jpg'));
%for i =3:5;
img=imread(list(i).name);
img=img(:,:,1);
b=img;
clear img;
i

imgb=multithresh(b,2);%perform a dual multiotsu
dual=imquantize(b,imgb);
rgb=label2rgb(dual);
rgbg=rgb2gray(rgb);
clear imgb;
clear dual;
clear rgb;

%simplify multithresh for gap and non-gap pixels:
rgbg(rgbg<=min(rgbg(:)))=0;
rgbg(rgbg>min(rgbg(:)))=255;

imgm=im2bw(b,0.5); %manual thresholding

%write images
imwrite(rgbg, (['multi','_',list(i).name,'.png']));
imwrite(imgm, (['man_128','_',list(i).name,'.png']));

%get GF per ring (row-wise) 
sz=size(rgbg);
rw=sz(1);%number of rows
cl=sz(2);%number of columns
zen=95; %zenith range set during scan. 0 (zenith) is the top; >90 means below the scan height
wd=5; %zenith ring width
nrw=zen/wd; %number of zenith rings
szr=round(rw/nrw,0); %size in nr of rows

res=zeros(nrw,cl);
res2=zeros(nrw,cl);
fr=zeros(nrw,1);
tw=zeros(nrw,1);

%get gf from manual and multi-otsu
for z=nrw:-1:1
    %a=(z-1)*szr;
    %b=a+szr;
    a=rw-((nrw-z)*szr);
    b=a-szr;
    if b<=0
    b=10;
    else
        b=b;
    end
    res(z,:) = mean(rgbg(b:a,:),1);
    gfotsu=1-mean(res,2)/255;
    res2(z,:) = mean(imgm(b:a,:),1);
    gfman=1-mean(res2,2);
    fr(z,:)=a;%nr rows from 
    tw(z,:)=b;%nr rows to
    %clear res res2
end



VZA=wd/2:wd:zen;
out= [repmat(i,nrw,1),VZA', fr, tw, gfman, gfotsu];
results=vertcat(results,out);

ID=repmat(list(i).name,nrw,1);
outs=strvcat(outs,ID);
end


%results=results(results(:,2)<=maxVZA,:);

rez=array2table(results);
rez.Properties.VariableNames(1:6) = {'ID','Zenith','from','to','GF_man','GF_aut'};
rez.File=outs(:,1:end);

%subset at specified maxVZA
maxVZA=75;
rez=rez(rez.Zenith<maxVZA,:);
writetable(rez,'gap_fraction.csv');